void hid_putc(char inChar) {
   gblHIDMessage[gblHIDMessageIndex++] = inChar;
   if ((inChar == '\n') || (gblHIDMessageIndex > REGISTER_SIZE)) {
      gblHIDMessage[0] = 1;  // packet type = message
      gblHIDMessage[1] = gblHIDMessageIndex;
      usb_put_packet(USB_HID_ENDPOINT, gblHIDMessage, USB_CONFIG_HID_TX_SIZE, USB_DTS_TOGGLE);
      gblHIDMessageIndex = 2;
      //beep();
   }
}
