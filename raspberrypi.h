// ========================================================
// Serial Buffer Constants

#define SERIAL_NO_DATA                  0
#define SERIAL_SUCCESS                  1
#define SERIAL_OVERFLOW                 2

#int_rda
void serial_isr(void);

#define RPI_WATCH_DOG_LIMIT            40
int gblRpiWatchDog = RPI_WATCH_DOG_LIMIT;

#define RPI_TX_BUFFER_SIZE  64
char gblRpiTxBuffer[RPI_TX_BUFFER_SIZE];
int1 gblRpiTxBufferIsFull=FALSE;
int gblRpiTxBufferPutIndex=0;
int gblRpiTxBufferGetIndex=0;

#define RPI_RX_BUFFER_SIZE  63 // 63 is the HID packet legth. We use this length so that we can send this
                               // buffer to the GoGo Widget via HID
// Two buffers are created, A and B
// They always alternate roles between the receiving buffer
// and the Read-from-buffer 
char gblRpiRxBufferA[RPI_RX_BUFFER_SIZE]; 
char gblRpiRxBufferB[RPI_RX_BUFFER_SIZE];

int1 gblRpiRxUsingBufferA = 1;
int  gblRpiRxBufferPutIndex = 0;
int  gblRpiWaitingForRxHeader = 1;   // 0=header already found, 1=waiting for header1, 2=waiting for header 2
int  gblRpiRxChecksum = 0;
//int32 gblRpiRxReadFlag = 0;  

int1 gblRpiNeedToCheckPacketType=0;
int gblRpiCurrentPacketType=0;
int gblRpiPacketLen=0;

// Key-value variables
#define RPI_REPORT_PACKET     2   // pakcet type 2 is the rpi status packet
#define RPI_KEY_VALUE_PACKET  7   // type 7 is key-value packet
#define RPI_KEY_BUFFER_COUNT   4
#define RPI_KEY_BUFFER_LEN    12
char gblRpiKey[RPI_KEY_BUFFER_COUNT][RPI_KEY_BUFFER_LEN]={0};
char gblRpiKeyValue[RPI_KEY_BUFFER_COUNT][RPI_KEY_BUFFER_LEN]={0};
char gblRpiCurrentKey[RPI_KEY_BUFFER_LEN]={0}; // holds the current key being used
char gblRpiTargetValue[RPI_KEY_BUFFER_LEN]={0}; // hosds the value we want to test

int1 gblRpiNeedToUpdateKeyValueBuffer =0;
int gblRpiKeyIndex = 0;
int1 gblRpiNeedGetPacketLen=0;


// ============================================
// Command constants
// ============================================
#define RPI_SHUTDOWN                      1
#define RPI_REBOOT                        2
#define RPI_CAMERA_CONTROL                10
#define RPI_FIND_FACE_CONTROL             11
#define RPI_TAKE_SNAPSHOT                 12
#define RPI_WIFI_CONNECT                  15
#define RPI_WIFI_DISCONNECT               16
#define RPI_EMAIL_CONFIG                  17
#define RPI_EMAIL_SEND                    18
#define RPI_SMS_SEND                      19
#define RPI_SET_TX_BUFFER                 20
#define RPI_RFID_INIT                     25
#define RPI_RFID_COMMAND                  26


// ============================================
// Register Map
// ============================================


#define RPI_RX_HEADER1 0x54
#define RPI_RX_HEADER2 0xFE

#define RPI_REG_PACKET_TYPE      0  // always = 2
#define RPI_REG_HW_VERSION       1
#define RPI_REG_CPU_LOAD         2  // in %
#define RPI_REG_CPU_TEMP         3  // in degrees C
#define RPI_REG_MEM_USED         4  // in %
#define RPI_REG_IP1              10  // if IP=192.168.1.10, IP1 = 192
#define RPI_REG_IP2              11
#define RPI_REG_IP3              12
#define RPI_REG_IP4              13
#define RPI_REG_WLAN_IP1         14  // if IP=192.168.1.10, IP1 = 192
#define RPI_REG_WLAN_IP2         15
#define RPI_REG_WLAN_IP3         16
#define RPI_REG_WLAN_IP4         17

#define RPI_REG_CAMERA_FLAGS     18
#define RPI_REG_SMS_FLAGS        19

#define RPI_REG_SCREEN_TAPPED_VALUE    20   // returns none 0 if the screen is tapped
#define RPI_SCREEN_TAP_X_POS_HB     = 21
#define RPI_SCREEN_TAP_X_POS_LB     = 22
#define RPI_SCREEN_TAP_Y_POS_HB     = 23
#define RPI_SCREEN_TAP_Y_POS_LB     = 24

#define RPI_WIFI_STATUS             = 25
#define RPI_EMAIL_STATUS            = 26
#define RPI_SMS_STATUS              = 27

#define RPI_REG_RFID_STATUS         = 28
#define RPI_REG_RFID_TAG_CONTENT    = 29

#define RPI_COMMAND_PACKET       5


#define SEND_MESSAGE_INT   1
#define SEND_MESSAGE_STR   2

// Bit definitions in the RPI_REG_CAMERA_FLAGS byte
#define RPI_CAMERA_IS_ON_BIT               0
#define RPI_CAMERA_IS_FINDING_FACE_BIT      1
#define RPI_CAMERA_FACE_FOUND_BIT               2

void rPiInit(void);
void rpiAddCmdToBuffer(char cmd);
void rpiAppendOutputBuffer(char inByte);
void rpiAppendStringFromStackToBuffer();
void rpiAppendIntToText(int16 value);
void rpiAppend16BitValue(int16 value);
void rpiCommandStart();
void rpiCommandEnd();
void rpiTrickleFlushTx();
#separate
void  DoRpiStuff();

#separate
char rpiTxBufferRead(byte *charPtr);
int1 rpiTxBufferEmpty();
char rpiTxBufferGetChar();

int1 rpiCameraIsOn(void);
int1 rpiIsFindingFace(void);
int1 rpiFaceFound(void);
int rpiScreenIsTapped(void);

void rpiWriteRxBuffer(int index, int value);
void rpiWriteRpiTxBuffer(int index, int value);

int rpiRFIDRead(void);
int1 rpiRFIDTagFound(void);
int1 rpiRFIDReaderFound(void);

signed int rpiKeyAvailable(char *inKey);  // returns key index if it is found in buffer
signed int rpiGetKeyValue(char *inKey); // returns the index of the key-value buffer

