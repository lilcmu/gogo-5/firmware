//
// evalopcode.c - Contains the Logo Operations
//
// Copyright (C) 2001-2007 Massachusetts Institute of Technology
// Contact   Arnan (Roger) Sipiatkiat [arnans@gmail.com]
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//


/////////////////////////////////////////////////////
//
//  Logo Virtual Machine
//
//  Written by Arnan (Roger) Sipitakiat
//
//  Logo running on the GoGo Board was created based
//  on the Cricket Logo.
//
//  Cricket Logo creators include:
//    Fred Martin
//    Brian Silverman
//    Mitchel Resnick
//    Robbie Berg
//
/////////////////////////////////////////////////////


#separate
void evalOpcode(unsigned char opcode) {

int i;
int16 opr1, opr2, opr3;
unsigned int16 genPurpose=0;

int16 stringEnd;
char strchr;

   //////////////////////////////////////////////////////////////////////////////
   /// if opcode is a prcedure call 
   
   if (opcode == CALL) {
   
   
      genPurpose=gblMemPtr+2;  // this is the return address. Since CALL is followed
                               // by two addres bytes, we skip them by adding 2
   
      // update the mem pointer to point at the procedure
      gblMemPtr = (((unsigned int16)fetchNextOpcode()) << 8) + fetchNextOpcode();
   
      // then fetch the new opcode
   
      // The first opcode in a procedure is the number of parameters.
      opr1 = fetchNextOpcode();
   
   //!   /// if the second msb is set -> tail recursion
   //!   if (opcode & 0b01000000) {
   //!      // - Because this is tail recursion, we have to overwrite preveous procedure inputs
   //!      //   with the new ones. This loop removes the old inputs.
   //!      // - In addition to the procedure input we have three extra entries: the data stack pointer ,
   //!      //   the procedure return address, and the procedure input base address in the input stack.
   //!      for (i=0 ; i < (opr1+3) ; i++) {
   //!         inputPop();
   //!      }
   //!   }
   
      // Pop the procedure inputs from the main data stack and move them
      // to the input stack
      for (i=0 ; i<opr1 ; i++) {
         inputPush(stkPop());
      }
      inputPush(gblStkPtr);   // save the data stack pointer (we use this with STOP opcode to clear the
                        // data stack of the current procedure.
      inputPush(genPurpose);  // save the return address
   
      inputPush(gblInputStkPtr - (opr1+2));   // pushes a proc input base address index.
                                     // you'll need to read the documentation
                                     // to fully understand why. Meanwhile, see how it
                                     // is used in case LTHING
                                     // - we add 2 because we also keep the data stack pointer and
                                     //   the return address in this stack.
   
   
      //printf("pushed loop adr = %LX, repeat count = %LX\r\n", gblLoopAddress, gblRepeatCount);
   
   
      return;
   }


////////////////////////////////////////////////////////////////////////////////////////

   switch (opcode) {
      case  CODE_END:
       gblLogoIsRunning = 0;
       output_low(RUN_LED);

       // clear thes variables just in case.
        gblLoopAddress = 0;
       gblRepeatCount = 0;
       break;
      
      case  NUM8:
          stkPush(fetchNextOpcode());
          break;
         
      case  NUM16:
          stkPush(((unsigned int16)fetchNextOpcode() << 8) + fetchNextOpcode());
          break;
          
      // a string data type has been added in v.4.0.104
      case STRING: 
          opr1 = fetchNextOpcode();  // this is the string length
          
          // ========================================
          // This section accesses the flash memory
          // directely instead of using fetchNextOpcode() 
          // in order to reverse the order of the chars
          // in the string, so that the order not reversed
          // when poped out
          // ========================================
          
          stringEnd = gblMemPtr + opr1-1;
          gblMemPtr = stringEnd + 1;
          for (i=0;i<opr1;i++) {
                       
            strchr = read_program_eeprom(FLASH_USER_PROGRAM_BASE_ADDRESS + stringEnd-i);
            strchr &= 0xff;
            stkPush(strchr);
          }
          stkPush(opr1); // put the string length on top of the stack
          break;
          
       
       
      // Start of list command
      // it is followed by a list length byte (0-255), the list itself
      // and an EOL command. The length includes the EOL, but not the LIST
      // command.
      case  LIST:
          stkPush(gblMemPtr+1); // push the address of the fist command
          gblMemPtr += fetchNextOpcode();
          //printf("LIST new gblMemPtr = %LX\r\n", gblMemPtr);
       break;
       
      // Long list: The same as LIST but with a 16-bit length (0-65535)
      // ** This byte code is not part of the Cricket Logo standard.
      case LONG_LIST:
         stkPush(gblMemPtr+2); // push the address of the first command
         gblMemPtr += (((unsigned int16)fetchNextOpcode() << 8) + fetchNextOpcode() );
         break;
       
      case  EOL:
       genPurpose = stkPop();
       if (genPurpose > gblMemPtr) {
          gblMemPtr = genPurpose;
        } else {
          gblMemPtr = genPurpose;
          gblRepeatCount = stkPop();   // repeat count
          if (gblRepeatCount > 1) { gblRepeatCount--; }
          if (gblRepeatCount != 1) {
             stkPush(gblRepeatCount);
             stkPush(gblMemPtr);
           }
        }
        gblCmdDelayCounter = 5; // add a 2.66*5 = 13.3 ms delay at then end of each loop.
                            // this increases the stability.
        
       //printf("EOL return address = %LX, loop address = %LX\r\n",gblMemPtr, gblLoopAddress);
       break;
      case  EOLR:
       if(stkPop()) {    // if condition is true
         stkPop();        // throw away the loop address
         gblMemPtr = stkPop(); // fetch the next command address
         } else { // if condition if false -> keep on looping.
          gblMemPtr = stkPop();
          stkPush(gblMemPtr);
          gblCmdDelayCounter = 5;  // add a 2.66 * 5 = 13.3 ms delay prevents the waituntil loop to execute too rapidly
                         // which has proven to cause some problems when reading
                         // sensor values.
                 
//          while (1) {output_high(USER_LED); delay_ms(50); output_low(USER_LED); delay_ms(50);}
                       
       }

       break;

     /////////////////////////////////////////////////////////////
     // retrieve procedure input
      case  LTHING:
          genPurpose = fetchNextOpcode();  // index of the input variable
          opr1 = inputPop();  // base address in the input stack
          inputPush(opr1);    // push the base address back to the stack.
          stkPush(gblInputStack[opr1 + genPurpose]);
       break;

     /////////////////////////////////////////////////////////////
     // return to the parent procedure
      case   STOP:
      case OUTPUT:


          if (opcode == OUTPUT) { genPurpose = stkPop(); } // this is the output value

       opr1 = inputPop();  // this is the proc-input stack base address
        gblMemPtr = inputPop(); // this is the return address
       opr2 = inputPop();  // this is the data stack index;




       //printf( "poped loop adr = %LX, repeat count = %LX\r\n", gblLoopAddress, gblRepeatCount);

       // remove any remaining data that belongs to the current procedure from the data stack
       // Usually this is important for the STOP opcode.
       while (gblStkPtr > opr2) { stkPop(); }

       // remove the procedure inputs from the input stack
       while (gblInputStkPtr > opr1) { inputPop(); }

          // Output will push the output to the stack
          if (opcode == OUTPUT) { stkPush(genPurpose); }

       break;



      case  REPEAT:
          gblLoopAddress = stkPop();
          gblRepeatCount = stkPop();

       // these will be poped by EOL
       stkPush(gblMemPtr);  // address after repeat is complete

          if (gblRepeatCount > 1) {
             stkPush(gblRepeatCount);
             stkPush(gblLoopAddress); // address while still repeating
             gblMemPtr = gblLoopAddress;
        } else if (gblRepeatCount == 1) {
           gblMemPtr = gblLoopAddress;
        } else {  // if loop count = 0
             gblMemPtr = stkPop();
        }
       break;
      case  COND_IF:
         opr1=stkPop();  // if true pointer address
         opr2=stkPop();  // condition
        //printf("if %LX goto %LX\r\n", opr2,opr1);
         if (opr2) {
            stkPush(gblMemPtr);
            gblMemPtr=opr1;
         }
         break;
      case  COND_IFELSE:
         opr1=stkPop(); // if false pointer address
         opr2=stkPop(); // if true pointer address
         opr3=stkPop(); // condition
         stkPush(gblMemPtr);
         if (opr3) {
            gblMemPtr=opr2;
         } else {
            gblMemPtr=opr1;
         }

       break;

      case IF_STATE_CHANGE:
         // executes the if statements only when the condition becomes true for the first time
         opr1=stkPop();  // if true pointer address
         opr2=stkPop();  // condition
         opr3=stkPop();  // the if id. Used to access a tracking buffer to determine when 
                         // the condition changes from false -> true

         // each byte in the gblStateChangeFlags[] array tracks 8 states
         if (opr2 && !bit_test(gblStateChangeFlags[opr3/8], opr3 % 8)) {
            stkPush(gblMemPtr);
            gblMemPtr=opr1;
            bit_set(gblStateChangeFlags[opr3/8], opr3 % 8);
         } else if (!opr2) {
            bit_clear(gblStateChangeFlags[opr3/8], opr3 % 8);
         }
      
      
         break;      
      
      
      case  BEEP:
          beep();
       break;
      case  NOTE:
       break;
      case  WAITUNTIL:
          gblLoopAddress = stkPop();

       // these will be poped by EOLR
       stkPush(gblMemPtr);  // address after repeat is complete
          stkPush(gblLoopAddress); // address while still repeating
          gblMemPtr = gblLoopAddress;

       break;
      case  LOOP:
         gblLoopAddress = stkPop(); // the begining of loop
         gblRepeatCount = 0; // disable this counter (loop forever)
        stkPush(0);   // this distinguishes LOOP from Repeat. (see EOL)
         stkPush(gblLoopAddress); // push loop address back into the stack
                           // so that EOL will loop
           gblMemPtr = gblLoopAddress;
       //printf("LOOP loopaddress = %LX\r\n", gblLoopAddress);
       break;
      case  WAIT:
          gblWaitCounter = stkPop();// the main() loop will pause until
                           // gblWaitCounter is 0. Timer1 ISR
                            // subtracts its value every 0.1 sec.
       break;

      case  TIMER:
         stkPush(gblTimer); // gblTimer increases every 1ms. See in RTCC interrupt
       break;
      case  RESETT:
         gblTimer = 0;
       break;

      // Send will transmit a 16 bit value via the serial port 
      case  SEND:
         genPurpose = stkPop();
         printf(active_comm_putc, "%c%c", genPurpose>>8, genPurpose & 0xff);
       break;
     






//!      ///////////////////////////////////////////////////////////////////////
//!      //  Global Array
//!
//!      case  ASET:
//!          opr2 = stkPop();      // this is the value to be stored
//!          opr1 = stkPop() * 2;   // this is the array index. Each entry is two bytes wide.
//!          genPurpose = ARRAY_BASE_ADDRESS + stkPop();  // this is the base address of the array.
//!
//!         #if defined(__PCM__)
//!
//!          write_ext_eeprom(genPurpose + opr1, opr2 & 0xff); // low byte
//!          write_ext_eeprom(genPurpose + opr1 + 1, opr2 >> 8); // high byte
//!
//!         #elif defined(__PCH__)
//!         
//!          FLASHSetByteAddress( (genPurpose + opr1)<<1);
//!          FLASHWrite(opr2);
//!         
//!         #endif
//!
//!       break;
//!      case  AGET:
//!          opr1 = stkPop() * 2;   // this is the array index. Each entry is two bytes wide.
//!          genPurpose = ARRAY_BASE_ADDRESS + stkPop();  // this is the base address of the array.
//!
//!          #if defined(__PCM__)
//!          
//!          opr2 = read_ext_eeprom(genPurpose + opr1);
//!          opr2 |= (int16)read_ext_eeprom(genPurpose + opr1 + 1) << 8;
//!
//!          #elif defined(__PCH__)
//!          
//!          opr2 = read_program_eeprom(genPurpose + opr1); 
//!          
//!          #endif
//!
//!          stkPush(opr2);
//!
//!
//!       break;
//!
//!      /////////////////////////////////////////////////////////////////////////
//!      //  Data collection commands
//!
//!      case  RECORD:
//!          genPurpose = stkPop();
//!
//!          // PCM parts (14 bit PICs like the 16F877) uses an external EEPROM
//!          // for data Logging storage
//!          #if defined(__PCM__)
//!         
//!          // record the data
//!          write_ext_eeprom(RECORD_BASE_ADDRESS + gblRecordPtr++, genPurpose & 0xff); // low byte
//!          write_ext_eeprom(RECORD_BASE_ADDRESS + gblRecordPtr++, genPurpose >> 8); // high byte
//!
//!          // save current record pointer location to the internal EEPROM
//!          // we use this for the automatic data upload to the PC
//!          write_ext_eeprom(MEM_PTR_LOG_BASE_ADDRESS, gblRecordPtr & 0xff);
//!          write_ext_eeprom(MEM_PTR_LOG_BASE_ADDRESS + 1, gblRecordPtr >> 8);
//!          
//!          
//!          // PCH parts (16 bit PICs like the 18F458) uses the internal flash
//!          #elif defined(__PCH__)
//!          
//!          // record the data
//!          FLASHSetByteAddress(RECORD_BASE_ADDRESS + ((gblRecordPtr++)<<1));
//!          FLASHWrite(genPurpose);
//!          
//!          // save current record pointer location 
//!          FLASHSetByteAddress(MEM_PTR_LOG_BASE_ADDRESS);
//!          FLASHWrite(gblRecordPtr);          
//!          
//!          #endif
//!
//!
//!       break;
//!
//!      case  RECALL:
//!
//!          #if defined(__PCM__)
//!
//!          genPurpose = read_ext_eeprom(RECORD_BASE_ADDRESS + gblRecordPtr++);
//!          genPurpose |= ((int16)read_ext_eeprom(RECORD_BASE_ADDRESS + gblRecordPtr++) << 8);
//!
//!         #elif defined (__PCH__)
//!         
//!         genPurpose = read_program_eeprom(RECORD_BASE_ADDRESS + (gblRecordPtr<<1) );
//!         gblRecordPtr++;
//!                //foo = read_program_eeprom(RECORD_BASE_ADDRESS + (counter<<1));
//!
//!         #endif
//!         
//!          stkPush(genPurpose);
//!       break;
//!       
//!      case  RESETDP:
//!       gblRecordPtr = 0;
//!       break;
//!      
//!      case  SETDP:
//!       gblRecordPtr = stkPop();
//!       break;
//!      
//!      case  ERASE:
//!          opr1 = stkPop() * 2;
//!          for (genPurpose=0 ; genPurpose<opr1 ; genPurpose++) {
//!
//!             #if defined(__PCM__)
//!             write_ext_eeprom(RECORD_BASE_ADDRESS + genPurpose, 0);
//!
//!             #elif defined(__PCH__)
//!             FLASHSetByteAddress(RECORD_BASE_ADDRESS + (genPurpose<<1));
//!             FLASHWrite(0);             
//!
//!             #endif
//!
//!
//!        }
//!        gblRecordPtr = 0;
//!       break;




     /////////////////////////////////////////////////////////////////
     //  Due to the PIC's memory architecture, we need to split this
     //  procedure so that it isn't too big to fit one memory segment.
      default:  evalOpcode2(opcode);
      };
}



#separate void evalOpcode2(unsigned char opcode) {

int i=0;
int16 opr1, opr2, opr3;
unsigned int16 genPurpose=0;
int foo;
char *strPtr;  // used with the show long string command
int strLen;    //  used with the show long string command
char displayModuleBuffer[33]; // 16x2 LCD display buffer. The 33 location is for the null terminating char

   switch (opcode) {




     // IR on the gogo USB receives commands from a SONY remote control. It
     // does not use the IR to communicate with a computer like the Cricket.
     case   IR:
       // wait for the new IR code to arrive
       while(!gblNewIRCodeHasArrivedFlag && gblLogoIsRunning) { delay_ms(1); }
       stkPush(gblIRCode);
       gblNewIRCodeHasArrivedFlag = 0;
       break;
     case   NEWIR:
       stkPush(gblNewIRCodeHasArrivedFlag);
       break;

     case   SERIAL:
       // wait for a new Serial byte to arrive
       while(!gblMostRecentlyReceivedByte && gblLogoIsRunning) { delay_us(500); }
       stkPush(gblMostRecentlyReceivedByte);
       gblNewByteHasArrivedFlag = 0;
       break;
     case   NEWSERIAL:
       stkPush(gblNewByteHasArrivedFlag);
       break;


     case  RANDOM:
          opr1 = stkPop();  // the upper limit 
          stkPush(rand() % opr1);
       break;

      case  OP_PLUS:
      case  OP_MINUS:
      case  OP_MULTIPLY:
      case  OP_DIVISION:
      case  OP_MODULO:
      case  OP_EQUAL:
      case  OP_GREATER:
      case  OP_LESS:
      case  OP_AND:
      case  OP_OR:
      case  OP_XOR:
      case  OP_LESS_OR_EQUAL:
      case  OP_GREATER_OR_EQUAL:

         opr2=stkPop();  // second operand
         opr1=stkPop();  // first operand
         //printf("op1=%LX\r\n",opr1);
         //printf("op2=%LX\r\n",opr2);
         switch (opcode) {
            case  OP_PLUS:
               opr1+=opr2;
               break;
            case  OP_MINUS:
               opr1-=opr2;
               break;
            case  OP_MULTIPLY:
               opr1*=opr2;
               break;
            case  OP_DIVISION:
               opr1/=opr2;
               break;
            case  OP_MODULO:
               opr1%=opr2;
               break;
            case  OP_EQUAL:
               opr1=(opr1==opr2);
               break;
            case  OP_GREATER:
               opr1=(opr1>opr2);
               break;
            case  OP_LESS:
               opr1=(opr1<opr2);
               break;
            case  OP_AND:
               opr1=(opr1&&opr2);
               break;
            case  OP_OR:
               opr1=(opr1||opr2);
               break;
            case  OP_XOR:
               opr1=(opr1^opr2);
               break;
            case OP_LESS_OR_EQUAL:
               opr1=(opr1<=opr2);
               break;
            case OP_GREATER_OR_EQUAL:
               opr1=(opr1>=opr2);
               break;
            };
       //printf("result=%LX\r\n", opr1);
       stkPush(opr1);
       break;

      case  OP_NOT:
       stkPush(!stkPop());
       break;


     ///////////////////////////////////////////////////////////////////////
     // Global variables
      case  SETGLOBAL:
          genPurpose = stkPop();  // this is the value
          globalVariables[stkPop()] = genPurpose;
       break;
      case  GETGLOBAL:
         stkPush(globalVariables[stkPop()]);
       break;


      case  WHEN:
       break;
      case  WHENOFF:
       break;
      case  M_A:
          gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00000001;  // set bit 0
       break;
      case  M_B:
          gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00000010;  // set bit 1
       break;
       

       
//!      case  M_AB:
//!          gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00000011;
//!       break;

      //////////////////////////////////////////////////////

     // Look at how M_ON, M_ONFOR, and M_OFF work carefully.
     // - M_ON, M_ONFOR starts by turning motors on.
     // - M_ON breaks right after while M_ONFOR continues.



      case  M_OFF:         i++;
      case  M_THATWAY:     i++;
      case  M_THISWAY:     i++;
      case  M_RD:          i++;
      case  BRAKE:         i++;
      case  M_ON:
      case  M_ONFOR:

// Moved to motorCortol() instead
//         SetMotorMode(MOTOR_NORMAL);
         MotorControl(i);

         if (opcode == M_ONFOR) {
           gblWaitCounter = stkPop(); // the main() loop will pause until
                               // gblWaitCounter is 0. Timer1 ISR
                               // subtracts its value every 0.1 sec.

            gblONFORNeedsToFinish = 1; // this will cause fetchNextOpcode()
         }

 //        printf( "i=%d\r\n",i);

         break;


     ////////////////////////////////////////////////////

      case  SETPOWER:
       SetMotorPower(stkPop());
       break;


      case TALK_TO_NODE:
         // this command is for use with the wireless gogo. It has not been
         // integrated into this firmware yet.
         stkPop();
         break;

      case ISON:
         // the parameter is the motor bits we want to check. If all are 
         // on then return true.
         // We do this using AND between the target bits and their on status
         // if the target bits don't change -> all bits are on -> return true
         opr1 = stkPop();
         stkPush(opr1 == (opr1 & gblMotorONOFF));
         break;
         
      case ISOFF:
         // see ISON for notes
         opr1 = stkPop();
         stkPush(opr1 == (opr1 & ~gblMotorONOFF));
         break;

      case ISTHISWAY:
         // see ISON for notes
         opr1 = stkPop();
         stkPush(opr1 == (opr1 & gblMotorDir));
         break;
         
      case ISTHATWAY:
         // see ISON for notes
         opr1 = stkPop();
         stkPush(opr1 == (opr1 & ~gblMotorDir));
         break;
         
      case GETPOWER:
         // need to convert 0-255 to 0-100
         stkPush((int)(((float)gblMtrDuty[stkPop()]/255)*100));
         break;
         



//!      case  M_C:
//!          gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00000100;  // set bit 2
//!       break;
//!      case  M_D:
//!          gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00001000;  // set bit 3
//!       break;
//!      case  M_CD:
//!          gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00001100;
//!       break;
//!      case  M_ABCD:
//!       gblDeviceRegister[REG_ACTIVE_MOTORS] = 0b00001111;
//!       break;
//      case  FASTSEND:
//       break;

      case  REALLY_STOP:
       
       stopLogoProcedures();
       break;

      case  EB:  // reads byte from memory
//!           #if defined(__PCM__)
//!              stkPush(read_ext_eeprom(stkPop()));
//!           #elif defined(__PCH__)
//!              stkPush(read_program_eeprom(stkPop()));
//!           #endif
       break;

      case  DB:  // deposit byte to memory
//!           /// Note: I have checked this code. I might have swapped opr1 and opr2
//!           opr1 = stkPop(); // value to write
//!           opr2 = stkPop(); // memory address
//!           
//!          #if defined(__PCM__)
//!             write_ext_eeprom(opr2,opr1);
//!
//!          #elif defined(__PCH__)
//!             FLASHSetByteAddress(opr2);
//!             FLASHWrite(opr1);             
//!
//!          #endif           
//!        
//!          // write_ext_eeprom(opr2,opr1);
       break;

      case  LOW_BYTE:  // returns low byte
           stkPush(stkPop() & 0xff);
       break;
      case  HIGH_BYTE:  // returns high byte
           stkPush(stkPop() >> 8);
       break;



//!      /// read sensor
//!      case  SENSOR1:
//!      case  SENSOR2:
//!      case  SENSOR3:
//!      case  SENSOR4:
//!      case  SENSOR5:
//!      case  SENSOR6:
//!      case  SENSOR7:
//!      case  SENSOR8:
//!
//!       // we need the following IF because the opcode for sensor1&2 are separate from the rest.
//!       // If this wasn't the case we could have just done .. i = opcode - SENSOR1;
//!       if (opcode < SENSOR3) { i = opcode - SENSOR1; }
//!       else { i = opcode - SENSOR3 + 2; }
//!
//!         stkPush(readSensor(i));  
//!       break;
//!

      /// read sensor
      case  READ_SENSOR:
         i = stkPop();   // i holds the sensor number 
         stkPush(readSensor(i-1));
         break;
      
      
//!      // read sensor and treat it as a on-off switch (0 or 1)
//!      case  SWITCH1:
//!      case  SWITCH2:
//!      case  SWITCH3:
//!      case  SWITCH4:
//!      case  SWITCH5:
//!      case  SWITCH6:
//!      case  SWITCH7:
//!      case  SWITCH8:
//!
//!       if (opcode < SWITCH3) { i = opcode - SWITCH1; }
//!       else { i = opcode - SWITCH3 + 2; }
//!
//!         stkPush(!(readSensor(i)>>9));
//!       break;
//!

      // read sensor and treat it as a on-off switch (0 or 1)
      case  READ_SWITCH:
         i = stkPop();   // the switch number (1-8)
         stkPush(!(readSensor(i-1)>>9));
       break;



     /////////////////////////////////////////////////////////////
     //  user LED control
     case ULED_ON:
         USER_LED_ON;
         break;
     case ULED_OFF:
         USER_LED_OFF;
         break;


     /////////////////////////////////////////////////////////////
     //  Servo controls

     case SERVO_SET_H:
     case SERVO_LT:
     case SERVO_RT:


         // Caution: SetMotorMode() must be called AFTER the
         // MotorControl() commands

         MotorControl(MTR_ON);
         MotorControl(MTR_THISWAY);
         SetMotorMode(MOTOR_SERVO);

         i = stkPop();

         if (opcode == SERVO_SET_H) {
            setServoDuty(i);
         } else if (opcode == SERVO_LT)
            ChangeMotorPower(i);
         else
            ChangeMotorPower(-1*i);

         break;

      case TALK_TO_MOTOR:
         opr1 = stkPop(); // this is the motor bits
         TalkToMotor (opr1);
         break;


      ///////////////////////////////////////////////////////////
      //
      //  I2C low-level commands 
      //

      case CL_I2C_START:
         gblI2CisBusy = 1;  // reserve the i2c bus
         i2c_start();
         break;

      case CL_I2C_STOP:
         i2c_stop();
         gblI2CisBusy = 0;  // releases the i2c bus
         break;

      case CL_I2C_WRITE:
         i2c_write(stkPop());
         break;

      case CL_I2C_READ:
         stkPush(i2c_read(stkPop()));
         break;

      ///////////////////////////////////////////////////////////
      // Initialize the RTC clock as follows:
      // - enable the clock. This needs to be cone every time the 
      //   RTC module looses power
      // - set time to 24 hrs mode (as opposed to the AM/PM mode)
      // - enable the square wave output on the RTC chip at 1Hz
      
      case RTC_INIT:
         rtcInit();         
         break;
      
      // Get one byte from the RTC clock
      
      case RTC_GET_ITEM:
         stkPush(rtcGetItem(stkPop()));
     
         break;
         
     ////////////////////////////////////////////////////////////
     // Display Module show command
     // Works with both the 7-Segment and 16x2 LCD modules
     
      case DISPLAY_SHOW:
         opr1 = stkPop(); // this tells us what to do
         if (opr1 == DISPLAY_CMD_SEND_VALUE) { // send a 16-bit value
            displayValue(stkPop());
            delay_us(300);   // must delay to allow the display to process the input value
                             // before sending any further i2c commands. This display module
                             // has to convert this number into a string and copy it to the
                             // display buffer. This is a time consuming process. Without this
                             // delay, it is possible to cause an overrun error.
                             
         } else if ((opr1 == DISPLAY_CMD_SEND_TEXT) | (opr1 == DISPLAY_CMD_SEND_LONG_TEXT)) { // send a 4-letter text (7-segment)

            strLen = stkPop();   // get the text length
            
            for (i=0;i<strLen;i++) {
               displayModuleBuffer[i] = stkPop();
            }
            displayModuleBuffer[strLen] = 0;  // null terminating character
            displayText(opr1, displayModuleBuffer); // send the string to the display
         }
         
         break;
 
      case DISPLAY_CLS:
         clearDisplay();
         break;
 
      case DISPLAY_GETPOS:
         stkPush(getDisplayPos());
         break;

      case DISPLAY_SETPOS:
         setDisplayPos(stkPop());
         break;

      case TALK_TO_7SEG_1:
         gblAutoDetectDisplays=0;
         gblDisplayAddress=DISPLAY_7SEG_ADDR_1;
         break;

      case TALK_TO_7SEG_2:
         gblAutoDetectDisplays=0;
         gblDisplayAddress=DISPLAY_7SEG_ADDR_2;
      
         break;

      case TALK_TO_LCD_1:
         gblAutoDetectDisplays=0;
         gblDisplayAddress=DISPLAY_LCD_ADDR_1;
      
         break;

      case TALK_TO_LCD_2:
         gblAutoDetectDisplays=0;
         gblDisplayAddress=DISPLAY_LCD_ADDR_2;
      
         break;

      case I2C_WRITE_REGISTER:
         // i2cWrite( i2c address, register Address, value)
         i2cWrite(stkPop(), stkPop(), stkPop());
         break;
         
      case I2C_READ_REGISTER:
         // i2cRead( i2c address, register address, &address of variable to store results)
         i2cRead(stkPop(), stkPop(), &foo);
         stkPush(foo);
         break;

      case SETTICKPERIOD:
         gblTickPeriod = stkPop();
         gblTickCounter = 0;
         gblTickTimer=0;
         break;
         
      case TICKCOUNT:
         stkPush(gblTickCounter);
         break;
         
      case CLEARTICK:
         gblTickCounter = 0;
         gblTickTimer=0;
         break;


      default:
         evalRpiOpcode(opcode);
 
   };

}

void evalRpiOpcode(unsigned char opcode) {

   int strLen, i;
   signed int keyIndex;

   switch (opcode) {
      case RPI_REBOOT:
         rpiAddCmdToBuffer(RPI_REBOOT);
         break;
         
      case RPI_SHUTDOWN:
         rpiAddCmdToBuffer(RPI_SHUTDOWN);
         break;
   
   
      case USE_CAMERA:
         rpiAddCmdToBuffer(USE_CAMERA);
         break;

      case CLOSE_CAMERA:
         rpiAddCmdToBuffer(CLOSE_CAMERA);
         break;
      case CAMERA_IS_ON:
         stkPush(rpiCameraIsOn());
         break;

      case START_FIND_FACE:
         rpiAddCmdToBuffer(START_FIND_FACE);
         break;
      case STOP_FIND_FACE:
         rpiAddCmdToBuffer(STOP_FIND_FACE);
         break;
      case IS_FINDING_FACE:
         stkPush(rpiIsFindingFace());
         break;
      case FACE_FOUND:   // facefound? -> see if a face is found in the image stream
         stkPush(rpiFaceFound());
         break;
       
      case TAKE_SNAPSHOT:
         rpiAddCmdToBuffer(TAKE_SNAPSHOT);
         break;
      
      case TAKE_PREVIEW_IMAGE:
         rpiAddCmdToBuffer(TAKE_PREVIEW_IMAGE);
         
      case USE_SMS:
         rpiAddCmdToBuffer(USE_SMS);
         break;
      case SEND_SMS:
         rpiCommandStart();
         
         // Add the command byte
         rpiAppendOutputBuffer(SEND_SMS);

         // Add the phone number string
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(',');

         // Add the SMS text
         rpiAppendStringFromStackToBuffer();
        
         rpiCommandEnd();
         
         break;

      case SEND_MAIL:
         
         rpiCommandStart();
         
         // Add the command byte
         rpiAppendOutputBuffer(SEND_MAIL);

         // Add the e-mail address
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(',');

         // Add the e-mail subject
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(',');
         
         // Add the e-mail body
         rpiAppendStringFromStackToBuffer();
        
         rpiCommandEnd();

         break;          

      case SEND_SNAPSHOT:
         break;
      
      case PLAY_SOUND:
         rpiCommandStart();
         
         // Add the command byte
         rpiAppendOutputBuffer(PLAY_SOUND);
         rpiAppendStringFromStackToBuffer();
         rpiCommandEnd();
         break;
      
      case STOP_SOUND:
         rpiAddCmdToBuffer(STOP_SOUND);
         break;   
      
      case SAY:
          rpiCommandStart();
         
         // Add the command byte
         rpiAppendOutputBuffer(SAY);
         rpiAppendStringFromStackToBuffer();
         rpiCommandEnd();
         break;     
        
      case SHOW_IMAGE:
         rpiCommandStart();
         // Add the command byte
         rpiAppendOutputBuffer(SHOW_IMAGE);
         rpiAppendStringFromStackToBuffer();
         rpiCommandEnd();
         break;         
            
     case SCREEN_TAPPED:
         stkPush(rpiScreenIsTapped());
         break;

     case WIFI_CONNECT:
         rpiCommandStart();
         
         // Add the command byte
         rpiAppendOutputBuffer(WIFI_CONNECT);

         // Add the e-mail address
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(',');

         // Add the e-mail subject
         rpiAppendStringFromStackToBuffer();
        
         rpiCommandEnd();

         break;       

      case WIFI_DISCONNECT:
         rpiAddCmdToBuffer(WIFI_DISCONNECT);
         break;

      case NEWRECORDFILE:
         rpiCommandStart();
         // Add the command byte
         rpiAppendOutputBuffer(NEWRECORDFILE);

         // Add the variable name
         rpiAppendStringFromStackToBuffer();
         rpiCommandEnd();
         
         break;
         
      case RECORD_TO_RPI:
         rpiCommandStart();
         // Add the command byte
         rpiAppendOutputBuffer(RECORD_TO_RPI);

         // Add the variable name
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(',');

         
         // Add the value
         rpiAppend16BitValue(stkPop());
         
         rpiCommandEnd();
     
     
         break;

      case SHOW_LOG_PLOT:
         rpiCommandStart();
         // Add the command byte
         rpiAppendOutputBuffer(SHOW_LOG_PLOT);

         // Add the variable name
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(';');  // we use a ';' because there can be commas in the string
         
         // Add the number of records to plot
         rpiAppend16BitValue(stkPop());
         rpiCommandEnd();
      
         break;


      case USE_RFID:
         rpiAddCmdToBuffer(USE_RFID);      
         break;
         
      case CLOSE_RFID:            
         rpiAddCmdToBuffer(CLOSE_RFID);
         break;
         
      case RFID_BEEP:             
         rpiAddCmdToBuffer(RFID_BEEP);
         break;
         
      case RFID_READ:             
         stkPush(rpiRFIDRead());
         break;
         
      case RFID_WRITE:            
         rpiCommandStart();
         // Add the command byte
         rpiAppendOutputBuffer(RFID_WRITE);
         rpiAppendOutputBuffer((int8)stkPop());
         rpiCommandEnd();
         break;
         
      case RFID_TAG_FOUND:        
         stkPush(rpiRFIDTagFound());
         break;
         
      case RFID_READER_FOUND:        
         stkPush(rpiRFIDReaderFound());
         break;


      case GET_KEY_VALUE:
        
         // fetch the key buffer string
         strLen = stkPop();
         for (i=0;i<strLen;i++) {
            gblRpiCurrentKey[i] = stkPop();   
         }
         gblRpiCurrentKey[i] = '\0'; // string terminator


         // push the index of the key-value buffer containing the 
         // key value. Note that -1 will be pushed if key is not found.
         // Since the stack is not signed, careful when checking for -1.
         stkPush(rpiGetKeyValue(gblRpiCurrentKey)); 

//         printf("index found for key %s = %d\r\n", gblRpiCurrentKey, rpiGetKeyValue(gblRpiCurrentKey));
         //printf("Found key %s at index = %u\r\n", gblRpiCurrentKey, rpiGetKeyValue(gblRpiCurrentKey));
         break;

      case GET_KEY_INT_VALUE:

         // fetch the key buffer string
         strLen = stkPop();
         for (i=0;i<strLen;i++) {
            gblRpiCurrentKey[i] = stkPop();   
         }
         gblRpiCurrentKey[i] = '\0'; // string terminator

         // push 65535 if the key is not found
         if ( (keyIndex = rpiGetKeyValue(gblRpiCurrentKey) ) == -1 )
            stkPush(65535);
         
//!         // convert the first two bytes into a 16 bit value;
//!         stkPush(((int16)gblRpiTargetValue[keyIndex][0] << 8)+gblRpiTargetValue[keyIndex][1]);
         
         stkPush(atol(gblRpiKeyValue[keyIndex]));
      
         break;


      case OP_KEY_COMPARE:
         
         
         // fetch the string constant to compare with
         strLen = stkPop();
         for (i=0;i<strLen;i++) {
            gblRpiTargetValue[i] = stkPop();
            
         }
         gblRpiTargetValue[i] = '\0'; // string terminator
         
         // fetch the key buffer index
         keyIndex = stkPop();
//         printf("index = %d\r\n", keyIndex);
         if ( (keyIndex == -1) || (keyIndex > RPI_KEY_BUFFER_COUNT)) {
            stkPush(0);
            break;
         }

//!         printf ("string val = %s\r\n", gblRpiTargetValue[i]);
//!         printf ("key val = %s\r\n", gblRpiKeyValue[keyIndex]);
//!         // compare the strings
         if (strcmp(gblRpiKeyValue[keyIndex], gblRpiTargetValue) == 0) {
            stkPush(1);
         } else {
            stkPush(0);
         }
        
         break;
         
      case CLEAR_KEYS:
         for (i=0;i<RPI_KEY_BUFFER_COUNT;i++) {
            gblRpiKey[i][0] = '\0';
            gblRpiKeyValue[i][0] = '\0';
            gblRpiKeyIndex = 0;  // reset the key pointer to the fist location
         }
         break;
         
         
         
         
      case SEND_MESSAGE:
         rpiCommandStart();
         // Add the command byte
         rpiAppendOutputBuffer(SEND_MESSAGE);

         // Add the variable name
         rpiAppendStringFromStackToBuffer();
         rpiAppendOutputBuffer(',');

         i = stkPop();
         if (i == SEND_MESSAGE_INT) {
            // Add the value
            rpiAppend16BitValue(stkPop());
         } else if (i == SEND_MESSAGE_STR) {
             rpiAppendStringFromStackToBuffer();
         }
        
         rpiCommandEnd();
     
    
         break;         

      default:
         Halt();

   }
}


