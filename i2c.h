void resetI2C();
int i2cWrite(int slaveAddress, int registerAddress, int registerValue);
int i2cWriteNbytes(int slaveAddress, int registerAddress, int len, int* registerValues);
int i2cRead(int slaveAddress, int registerAddress, int *registerValue);
int i2cReadNbytes(int slaveAddress, int registerAddress, int len, int *readBuffer);

#define SUCCESS      1
#define NOT_SUCCESS  0
