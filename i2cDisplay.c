//
// evalopcode.c - Display module plug-in routines
//
// Copyright (C) 2001-2007 Massachusetts Institute of Technology
// Contact   Arnan (Roger) Sipiatkiat [arnans@gmail.com]
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//



int displayPing() {
   int inbyte=0;
   int found=0;
   int i;
   int currentAddress;
   int displayAddress=0;

   
   // probe for any connected displays
   for (i=0;i<4;i++) {
      // fetch a display address to probe
      currentAddress = gblDisplayAddressList[i];
      ///disable_interrupts(GLOBAL);
      i2c_start();
      // We write to the Display address and check the return value
      // if i2c_write returns 0 -> it means we got an ACK back
      // so it means the display module is present.
      found = i2c_write(currentAddress);
      found = !found;
  
      if (found) {
         displayAddress = currentAddress;
         i2c_write(DISPLAY_CMD_PING);  // tell the slave we are just pinging.
         i2c_stop();
         ///enable_interrupts(GLOBAL);
         break;
      }
      i2c_stop();
      ///enable_interrupts(GLOBAL);
      
      delay_us(10); // delay to give time to other interrupts to
                    // do their work.
   }


   return(displayAddress);
   
}

void displayValue(int16 val) {

   int16 temp;

      // =================================================
      // show the value on the built-in 7-segment display
      generate7SegDisplayBuffer(val);

      // =================================================
      // show on an external i2c display
   
      i2c_start();
   
      // We write to the Display address and check the return value
      // if i2c_write returns 0 -> it means we got an ACK back
      // so it means the display module is present.
      if (i2c_write(gblDisplayAddress)) {
         ///enable_interrupts(GLOBAL);
         resetI2C();
         return;
      } 
      
      i2c_write(DISPLAY_CMD_SEND_VALUE);
      i2c_write((int)(val>>8));  // high byte
      i2c_write((int)(val & 0xFF));  // low byte
      i2c_stop();
   
        
}


//!void displayText(char ch1, char ch2, char ch3, char ch4) {
//!
//!   // show on external 7-segment 
//!   if (gblUseOnBoard7Segment == 0) {
//!   
//!      i2c_start();
//!   
//!      // We write to the Display address and check the return value
//!      // if i2c_write returns 0 -> it means we got an ACK back
//!      // so it means the display module is present.
//!      if (i2c_write(gblDisplayAddress)) {
//!         ///enable_interrupts(GLOBAL);
//!         return;
//!      } 
//!      
//!      i2c_write(DISPLAY_CMD_SEND_TEXT);
//!      i2c_write(ch4);  
//!      i2c_write(ch3);  
//!      i2c_write(ch2);  
//!      i2c_write(ch1);  
//!      i2c_stop();
//!   
//!   }
//!   // show on built-in 7-segment display
//!   else if (gblUseOnBoard7Segment == 1) {
//!      showOnBuiltIn7Segment(ch1,ch2,ch3,ch4);
//!   }
//!}

void displayText(mode, char *text) {
   signed int i=0;
   int strLen=0;
   char builtInDisplayBuffer[4];

      // =================================================
      // show the value on the built-in 7-segment display
      for(i=0;i<4;i++)
         {
            if (text[i] == '\0') {
                  while (i++<4) {
                     builtInDisplayBuffer[i] = ' ';  // ascii 32 = space
                  }
            } else {
               builtInDisplayBuffer[i] = text[i];
            }
         }   
      generate7SegmentDisplayBufferFromStirng(builtInDisplayBuffer);

      // =================================================
      // show on an external i2c display


     
      // if we are using the 16x2 module -> make sure we always use
      // the long text method.
      if ((gblDisplayAddress == 0xb4) | (gblDisplayAddress == 0xb6)) {
         mode = DISPLAY_CMD_SEND_LONG_TEXT;
      }
   
 
      i2c_start();
   
      // We write to the Display address and check the return value
      // if i2c_write returns 0 -> it means we got an ACK back
      // so it means the display module is present.
      if (i2c_write(gblDisplayAddress)) {
         ///enable_interrupts(GLOBAL);
         return;
      } 
      
      // mode is either 
      // - DISPLAY_CMD_SEND_LONG_TEXT
      // - DISPLAY_CMD_SEND_TEXT -> for the 7-segment display
      i2c_write(mode);
   
      if (mode == DISPLAY_CMD_SEND_LONG_TEXT) {
         // find the string len

         for (i=0 ; text[i] != '\0'; i++) {
            if (i>31) { break; }  // just in case there is no '\0'
            i2c_write(text[i]);
         }
         i2c_write('\0');


///////    Send string to display in reverse order
//!         for(strLen=0;text[strLen] != '\0';strLen++) ;
//!         
//!         // write the display string (in reverse order)
//!         for(i=strLen;i>0;i--) {
//!            i2c_write(text[i-1]);     
//!         }
//!         i2c_write('\0');   
   
     
      } else {
   
         // check the string and make sure we have 4 valid characters
         for(i=0;i<4;i++)
         {
            if (text[i] == '\0') {
                  while (i++<4) {
                     i2c_write(32);  // ascii 32 = space
                  }
            } else {
               i2c_write(text[i]);
            }
         }  
      } 
     

      i2c_stop();
   
   
}




int gblDisplaySensorCounter=0;  // tracks which sensor we have to send next

void displaySendSensors(void) {

   int16 curVal;
   int temp;


   ///disable_interrupts(GLOBAL); 

   curVal = readSensor(gblSensorPortMap[gblDisplaySensorCounter]);
   
   // send sensor values only if it is less than 1020. This is 
   // to minimize the i2c traffic. We assume a value >= 1020 
   // means there is no sensor attached to the port.
   // But always send at least sensor1's value.
   if ( (curVal < 1020) || (gblDisplaySensorCounter == 0)) {

      // send command
      i2c_start(); 
      i2c_write(gblDisplayAddress); 
      i2c_write(DISPLAY_CMD_UPDATE_SENSORS);    

      // send high byte 
      temp =  (int)(curVal >> 8) | (gblDisplaySensorCounter<<5);
      i2c_write (temp);
      // low byte
      i2c_write( (int)(curVal & 0xff) );
      i2c_stop();       
//      delay_us(400);  // must wait here
      
   }
   gblDisplaySensorCounter++;
   gblDisplaySensorCounter %= 8;

   ///enable_interrupts(GLOBAL); 
   
}


// ===================================================================
// LCD Specific commands

void clearDisplay(){
   ///disable_interrupts(GLOBAL);
   i2c_start();
   i2c_write(gblDisplayAddress);
   i2c_write(DISPLAY_CMD_CLS);
   i2c_stop();
   ///enable_interrupts(GLOBAL);
}

int getDisplayPos(){
   int pos=0;
   ///disable_interrupts(GLOBAL);
   i2c_start();
   i2c_write(gblDisplayAddress|0x01);   
   pos=i2c_read(0);
   pos++;
   i2c_stop();
   ///enable_interrupts(GLOBAL);
 
   return pos;
}


void setDisplayPos(int pos){
   ///disable_interrupts(GLOBAL);
   i2c_start();
   i2c_write(gblDisplayAddress);
   i2c_write(DISPLAY_CMD_SETPOS);
   i2c_write(pos);   // make the position start at 0  
   i2c_stop();
   ///enable_interrupts(GLOBAL);
}



