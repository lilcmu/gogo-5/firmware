

///////////////////////////////////////////////////////
// 
//  Receiving data from the Raspberry Pi
//
///////////////////////////////////////////////////////

#int_rda
void serial_isr(void)
{

   char inByte;

   inByte = getchar();
   
   /// if waiting for the record header
   if (gblRpiWaitingForRxHeader) {
      // if matching header1
      if ((gblRpiWaitingForRxHeader == 1) && (inByte == RPI_RX_HEADER1)) {
         gblRpiWaitingForRxHeader=2;
         
      // if matching header2
      } else if ((gblRpiWaitingForRxHeader == 2) && (inByte == RPI_RX_HEADER2)) {
         gblRpiWaitingForRxHeader=0;
         gblRpiNeedToCheckPacketType = 1;      
     
      
      // else -> reset and wait for header1
      } else {
         gblRpiWaitingForRxHeader=1;
      }
   } 
   
   // if receiving the record content
   else {

         // if this is the first byte of a packet, which is the packet type
         if (gblRpiNeedToCheckPacketType) {
            gblRpiCurrentPacketType = inByte;
            if (gblRpiCurrentPacketType == RPI_REPORT_PACKET) {
               gblRpiPacketLen = RPI_RX_BUFFER_SIZE;
            } else if (gblRpiCurrentPacketType == RPI_KEY_VALUE_PACKET) {
               gblRpiNeedGetPacketLen = 1;
            }
            gblRpiNeedToCheckPacketType = 0;

         // if this is the packet length byte

         } else if (gblRpiNeedGetPacketLen) {
            gblRpiPacketLen = inByte;
            gblRpiNeedGetPacketLen = 0;
          
       
         // if we have reached the end of the record
         } else if (gblRpiRxBufferPutIndex == gblRpiPacketLen) {
          
           
            // if the checksum is correct
            if (gblRpiRxChecksum == inByte) {
               output_toggle(USER_LED);   
               // Alternate the role of buffer A and B
               gblRpiRxUsingBufferA = !gblRpiRxUsingBufferA;
               gblRpiWatchDog = 0; // when < RPI_WATCH_DOG_LIMIT -> we are still
                                   // getting new packets from the Rpi.
            }
            // reset rx variables
            gblRpiRxChecksum = 0;
            gblRpiRxBufferPutIndex = 0;
            gblRpiWaitingForRxHeader = 1;
            
            if (gblRpiCurrentPacketType == RPI_KEY_VALUE_PACKET) {
               gblRpiNeedToUpdateKeyValueBuffer= 1; // used in main()
            }
            return;            
         } 
         
         // put the data into the rx buffer
         if (gblRpiRxUsingBufferA) {
            gblRpiRxBufferA[gblRpiRxBufferPutIndex++] = inByte;
         } else{ 
            gblRpiRxBufferB[gblRpiRxBufferPutIndex++] = inByte;
         }
         
         gblRpiRxChecksum += inByte;
         
      }
  

   

}

// read from the input buffer sent from the Raspberry Pi

char rpiReadRxBuffer(int index) {
   // Check which buffer to read from
   // if Buffer A is being used for Rx -> we read from Buffer B
   if (gblRpiRxUsingBufferA) {
      return gblRpiRxBufferB[index];
   } else {
      return gblRpiRxBufferA[index];
   }
}

void rpiWriteRxBuffer(int index, int value) {

      gblRpiRxBufferB[index] = value;
      gblRpiRxBufferA[index] = value;
   

}

// write to the Raspberry Pi's Tx buffer (which will become
// the Rx buffer on the gogo). This is useful when resetting
// flags on the buffer. For example, tell the screen tapped
// flag to become 0 after we have read it in the Logo program

void rpiWriteRpiTxBuffer(int index, int value) {

         rpiCommandStart();
         rpiAppendOutputBuffer(RPI_SET_TX_BUFFER);
         rpiAppendOutputBuffer(index);
         rpiAppendOutputBuffer(value);
         rpiCommandEnd();   

}



///////////////////////////////////////////////////////
// 
//  Sending data to the Raspberry Pi
//
///////////////////////////////////////////////////////



// add a single byte command to the output buffer
void rpiAddCmdToBuffer(char cmd) {
         rpiCommandStart();
         rpiAppendOutputBuffer(cmd);
         rpiCommandEnd();

}

void rpiCommandStart() {
   // adds a header byte
   rpiAppendOutputBuffer(RPI_COMMAND_PACKET);

}

void rpiCommandEnd() {
   rpiAppendOutputBuffer('\r');
   rpiAppendOutputBuffer('\n');
}


void rpiAppendOutputBuffer(char inByte) {

   if (gblRpiTxBufferIsFull == FALSE)
   {
      gblRpiTxBuffer[gblRpiTxBufferPutIndex] = inByte;         
      gblRpiTxBufferPutIndex++;
      if (gblRpiTxBufferPutIndex >= RPI_TX_BUFFER_SIZE)
         gblRpiTxBufferPutIndex = 0;   
      
      if (gblRpiTxBufferPutIndex == gblRpiTxBufferGetIndex)
         gblRpiTxBufferIsFull = TRUE;
     
   }
   
}

/// pop a string out of the Logo stack and put it on the output buffer
void rpiAppendStringFromStackToBuffer() {
   int strLen, i;

   strLen = stkPop();   // get the text length
   for (i=1;i<=strLen;i++) {
      rpiAppendOutputBuffer(stkPop());
   }

}

// converts int to string and add the bytes to the output buffer
// i.e. 123 gets converted to "123" and three bytes will be added
// to the output buffer (instad of one if int is used).
void rpiAppendIntToText(int16 value) {
   int currentDigit;
   int values[5]={0};   // largest value is 65535, which has 5 digits
   int index=0;
   
   // we put the text into an array before sending it so that the text
   // is not inverted.
   do {
      currentDigit = value - ((value/10)*10);
      value /= 10;
      values[index++] = currentDigit;
   } while (value > 0);

   do {
      rpiAppendOutputBuffer(48 + values[--index]);  // 48 is ASCII for "0"
   } while (index > 0);

}

void rpiAppend16BitValue(int16 value) {
//   rpiAppendOutputBuffer(value>>8);  // high byte 
//   rpiAppendOutputBuffer(value&0xff); // low byte

   rpiAppendIntToText(value);

}


// At Baud Rate = 115200 bps one character takes about 83 uS
// to send. To makes sure we don't occupy too much
// time continuously for this Tx process for long command strings,
// Trickle flush allows only TRICKLE_SIZE bytes to be sent 
// at a time. The MCU then goes on its bussiness and returns to
// Trickle flush again when it is ready. 

#define TRICKLE_SIZE    5

void rpiTrickleFlushTx() {
   
   int i;
   
   for (i=0;i<TRICKLE_SIZE;i++) {
      if (rpiTxBufferEmpty()) { break; }
      putc(rpiTxBufferGetChar());
   }
   
   

}

void updateKeyValueBuffer() {

      char *bufferPtr;
      char keyString[RPI_KEY_BUFFER_LEN]={0};
      int keyStringIndex=0;
      int j;
      signed int keyIndex=0;
    
      // point to the current Rx buffer
      if (gblRpiRxUsingBufferA) {
         bufferPtr = gblRpiRxBufferB;
      } else{ 
         bufferPtr = gblRpiRxBufferA;
      }      

      bufferPtr+=2; // skip the packet-type, len bytes
      // fetch the key string
      for (keyStringIndex=0;*(bufferPtr+keyStringIndex) != ',';keyStringIndex++) {
         keyString[keyStringIndex] = *(bufferPtr+keyStringIndex);         
      }
      
      // check if key string does not exists -> add one
      if ( (keyIndex = rpiKeyAvailable(keyString)) == -1) {
         gblRpiKeyIndex = ++gblRpiKeyIndex % RPI_KEY_BUFFER_COUNT;
         keyIndex = gblRpiKeyIndex;
         j=0;
         while ((gblRpiKey[keyIndex][j] = keyString[j]) != 0) {
            j++;
         }
      }
      
      // write the key value
      j=0;  
      while ((gblRpiKeyValue[keyIndex][j] = *(bufferPtr+keyStringIndex+j+1)) != 0) { //+1 to skipp the ','
         j++;
      }
      
      //printf ("k=%s, v=%s.\r\n", gblRpiKey[keyIndex], gblRpiKeyValue[keyIndex]);

}

#separate
void  DoRpiStuff() {
   
   // Trickle flush the Tx buffer to the raspberry pi.
   if (!rpiTxBufferEmpty()) {
      rpiTrickleFlushTx();
   }

   if (gblRpiNeedToUpdateKeyValueBuffer) {
         updateKeyValueBuffer();
         gblRpiNeedToUpdateKeyValueBuffer = 0;
       
   }


}

//////////////////////////////////////////////////////////////////////////////
// 
// Fetch a character from the "GoGo->Rpi" Tx serial buffer
//
//////////////////////////////////////////////////////////////////////////////
#separate
char rpiTxBufferRead(byte *charPtr)
{
   int errorCode;
   
   if (gblRpiTxBufferGetIndex == gblRpiTxBufferPutIndex)   
   {
       errorCode = SERIAL_NO_DATA;      
       *charPtr = 0;
   }
   else
   {
      *charPtr = gblRpiTxBuffer[gblRpiTxBufferGetIndex];
      gblRpiTxBufferGetIndex++;
      if (gblRpiTxBufferGetIndex >= RPI_TX_BUFFER_SIZE)
      {  gblRpiTxBufferGetIndex = 0;
      }
      errorCode = SERIAL_SUCCESS;   
   }
   return(errorCode);
}


int1 rpiTxBufferEmpty() {
   return(gblRpiTxBufferPutIndex == gblRpiTxBufferGetIndex);
}

char rpiTxBufferGetChar() {
   char foo;
   
   rpiTxBufferRead(&foo);
   return(foo);
   
}

void rPiInit(void) {
   gblRpiRxBufferA[0] = 2;
   gblRpiRxBufferB[0] = 2;
   

}


///////////////////////////////////////////////////////
// 
//  Worker functions
//
///////////////////////////////////////////////////////

int1 rpiIsFindingFace(void) {
   int1 output;
   
   output = bit_test(rpiReadRxBuffer(RPI_REG_CAMERA_FLAGS),RPI_CAMERA_IS_FINDING_FACE_BIT);
   return(output);
}

int1 rpiCameraIsOn(void) {
   int1 output;
   
   output = bit_test(rpiReadRxBuffer(RPI_REG_CAMERA_FLAGS),RPI_CAMERA_IS_ON_BIT);
   return(output);
}

int1 rpiFaceFound(void) {
   int1 output;
   
   output = bit_test(rpiReadRxBuffer(RPI_REG_CAMERA_FLAGS),RPI_CAMERA_FACE_FOUND_BIT);
   return(output);
}

int rpiScreenIsTapped(void) {
   int8 output;

   output = rpiReadRxBuffer(RPI_REG_SCREEN_TAPPED_VALUE);
   
   if (output != 0) {
      // clear the flag both on the gogo and the rpi
      rpiWriteRpiTxBuffer(RPI_REG_SCREEN_TAPPED_VALUE, 0);
      rpiWriteRxBuffer(RPI_REG_SCREEN_TAPPED_VALUE, 0);
      
   }
   return(output);

}


int rpiRFIDRead(void) {
//   return(rpiReadRxBuffer(RPI_REG_RFID_TAG_CONTENT));
   return(rpiReadRxBuffer(29));   
}

int1 rpiRFIDTagFound(void) {
   // Bit 1 of the RFID status is the Tag status (0=no tag, 1=tag found)
//   return(bit_test( rpiReadRxBuffer(RPI_REG_RFID_STATUS), 1) );
   return(bit_test( rpiReadRxBuffer(28), 1) );

}

int1 rpiRFIDReaderFound(void) {
   // Bit 0 of the RFID status is the reader status (0=found, 1=not found)
   return(bit_test( rpiReadRxBuffer(28), 0) );


}


// ========================================================
//  Key - Value commands
// ========================================================

// returns true if key is found in buffer
signed int rpiKeyAvailable(char *inKey) {
   int i;
   
   for (i=0 ; i<RPI_KEY_BUFFER_COUNT ; i++) {
//!      printf("comparing %s, %s\r\n", gblRpiKey[i],nKey);
//!      printf("len1 = %u, len2 = %u", strlen(gblRpiKey[i]), strlen(inKey));
//!      printf("result = %d\r\n", strcmp(gblRpiKey[i], inKey));
      if (strcmp(gblRpiKey[i],inKey) == 0) {
         return(i);
      }
   }
   return(-1);
}

// returns the index of the key-value buffer, -1 for not found
signed int rpiGetKeyValue(char *inKey) {
   return(rpiKeyAvailable(inKey));
}


